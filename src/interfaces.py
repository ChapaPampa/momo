#! /usr/bin/env  python3

class Leg():
    """
    .. todo:: Fill up this class.
    """

class Tripod():
    """
    Tripod interface of the robot.
    """

    def __init__(self, MH, MV, ME,
                       RH, RV, RE,
                       FH, FV, FE):
        """
        Create a tripod interface.

        :param MH: The horizontal middle servomotor of the tripod.
        :param MV: The vertical middle servomotor of the tripod.
        :param ME: The external middle servomotor of the tripod.
        :param RH: The horizontal rear servomotor of the tripod.
        :param RV: The vertical rear servomotor of the tripod.
        :param RE: The external rear servomotor of the tripod.
        :param FH: The horizontal front servomotor of the tripod.
        :param FV: The vertical front servomotor of the tripod.
        :param FE: The external front servomotor of the tripod.

        =========  =========================================
        Attribute  Description
        =========  =========================================
        all        All servomotors in the tripod.
        H          All horizontal servomotors in the tripod.
        V          All vertical servomotors in the tripod.
        E          All external servomotors in the tripod.
        =========  =========================================
        """

        self.all = [MH, MV, ME,
                    RH, RV, RE,
                    FH, FV, FE]
        self.H = [MH, RH, FH]
        self.V = [MV, RV, FV]
        self.E = [ME, RE, FE]

    Hmax = 1700
    Hmin = 1300
    Vhigh = 2300
    Vlow = 2000
    forward = [
            [1500, Vlow, 2000],
            [Hmin, Vlow, 2000],
            [1500, Vhigh, 2200],
            [Hmax, Vlow, 2000]
            ]

    backward = [
            [1500, Vlow, 2000],
            [Hmax, Vlow, 2000],
            [1500, Vhigh, 2200],
            [Hmin, Vlow, 2000],
            ]

    def set_mirror(self, HLvalue, VLvalue, ELvalue, HRvalue, VRvalue, ERvalue):
        """
        Set the tripod position by moving the tripod's legs.
        """
        for i in self.H:
            i.set_mirror(HLvalue if i.num > 15 else HRvalue)
        for i in self.V:
            i.set_mirror(VLvalue if i.num > 15 else VRvalue)
        for i in self.E:
            i.set_mirror(ELvalue if i.num > 15 else ERvalue)

    def set_antagonist(self, HLvalue, VLvalue, ELvalue, HRvalue, VRvalue, ERvalue):
        for i in self.H:
            i.set_antagonist(HLvalue if i.num > 15 else HRvalue)
        for i in self.V:
            i.set_mirror(VLvalue if i.num > 15 else VRvalue)
        for i in self.E:
            i.set_mirror(ELvalue if i.num > 15 else ERvalue)
