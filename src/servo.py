#! /usr/bin/env  python3

from abc import ABCMeta, abstractmethod

MIN_VALUE = 600
"""
Min Hard Angle
"""
MAX_VALUE = 2400
"""
Max Hard Angle
"""

class Servo():
    """
    Servomotor abstract class should not be used directly.
    """

    __metaclass__ = ABCMeta
    def __init__(self, num, center = 1500, init = 1500):
        """
        Create a servomotor.

        :param num: The port number where the servo is plugged in.
        """
        self.num = num
        self.command = "#" + str(num) + " P{0} "
        self.angle = -1
        self.next_angle = init
        self.offset = min(100, max(-100, 1500 - center))

    def get_command(self):
        """
        Generate a command that move the servo next_value position.
        """
        if self.angle == self.next_angle:
            self.angle = self.next_angle
            return ""
        else:
            self.angle = self.next_angle
            return self.command.format(self.next_angle)

    def get_calibration_command(self):
        return self.command.format("O" + str(self.offset))

    @abstractmethod
    def set_mirror(self, value):
        """
        Update the next value of the servo.

        :param value: The wanted servo position.
        """
        self.next_angle = value

    def set_antagonist(self, value):
        self.next_angle = max(MIN_VALUE, min(MAX_VALUE, value))

    def __str__(self):
        return self.command

class Rservo(Servo):
    """
    Servomotor on the right of the robot.
    """

    def set_mirror(self, value):
        value = max(MIN_VALUE, min(MAX_VALUE, 3000 - value))
        super().set_mirror(value)

class Lservo(Servo):
    """
    Servomotor on the left of the robot.
    """

    def set_mirror(self, value):
        value = max(MIN_VALUE, min(MAX_VALUE, value))
        super().set_mirror(value)
