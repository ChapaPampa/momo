#! /usr/bin/env  python3

from enum import Enum

MIN_SPEED = 0
"""
The minimum speed.
"""

MAX_SPEED = 3
"""
The maximum speed.
"""

class Translation(Enum):
    """
    Enum that describe the moving state of the robot.
    """
    none = 0
    forward = 1
    backward = 2
    right = 3
    left = 4
    forward_right = 5
    forward_left = 6
    backward_right = 7
    backward_left = 8

class Orientation(Enum):
    """
    Enum that describe the orientation state of the robot.
    """
    none = 0
    yaw_right = 1
    yaw_left = 2
    roll_right = 3
    roll_left = 4
    pitch_forward = 5
    pitch_backward = 6

class State():
    """
    Describe a robot state.
    """

    def __init__(self, translation = Translation.none, orientation = Orientation.none, speed = 1):
        """
        Create a state.

        :param translation: The desired translation during the unit move.
        :param orientation: The desired orientation during the unit move.
        """
        self.translation = translation
        self.orientation = orientation
        if speed <= MIN_SPEED:
            self.speed = MIN_SPEED
        elif speed >= MAX_SPEED:
            self.speed = MAX_SPEED
        else:
            self.speed = speed

    def create(self, x, size, width, height):
        if size > (width * height) / 16:
            self.translation = Translation.backward
        elif size < (width * height) / 64:
            self.translation = Translation.forward
        else:
            self.translation = Translation.none

        if x < width * 1 / 7:
            if self.translation == Translation.backward:
                self.orientation = Orientation.yaw_right
            else:
                self.orientation = Orientation.yaw_left
        elif x > width * 6 / 7:
            if self.translation == Translation.backward:
                self.orientation = Orientation.yaw_left
            else:
                self.orientation = Orientation.yaw_right
        else:
            self.orientation = Orientation.none
        self.speed = 1
        print(self)
        return self


    def merge(self, state):
        """
        Merge the actual state with the state passed in parameter.
        The current state prior the state passed in parameter.

        :param state: The state to merge with the actual one.
        """
        if state.translation != Translation.none:
            self.translation = state.translation
        if state.orientation != Orientation.none:
            self.orientation = state.orientation
        self.speed = state.speed

    def __str__(self):
        return str(self.translation) + "\t" + str(self.orientation) + "\t" + str(self.speed)
