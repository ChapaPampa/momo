#! /usr/bin/env  python3

import serial
import sys
import time
from threading import Thread
from src.servo import *
from src.event import *
from src.utils import *
from src.interfaces import *
from src.controller import *

class Hexapod(Thread):
    """
    Hexapod abstraction.
    Should be launched with the start method inherited from the Thread class.

    Servo Dispositon:
    ::

       FLE                FRONT                FRE
           \            ---------            /
             FLV -- FLH |   _   | FRH -- FRV
                        |  | |  |
       MLE - MLV -- MLH |  |_|  | MRH -- MRV - MRE
                        |       |
             RLV -- RLH |   -   | RRH -- RRV
           /            ---------            \
       RLE                REAR                 RRE
    """

    def __init__(self, serial_path = "/dev/ttyAMA0", baud_rate = 115200, timeout = 1.0, interval = 0.3):
        """
        Create an hexapod.

        :param serial_path: Path to the serial port where the SSC32-U is connected.
        :param baud_rate: The baud-rate of the SSC32-U.
        :param timeout: The timeout of serial communication.
        :param interval: Event check time interval in seconds.
        
        ==========  ===========================================================
        Attributes  Descriptions
        ==========  ===========================================================
        ssc32       The serial file descriptor to the SSC32-U board.
        state       The current state of the robot.
        tick        The current tick of the robot, represant the lifetime of\
                    the robot. It is used to find what partial move to execute.
        RRH         The Rear Right Horizontal servomotor.
        RRV         The Rear Right Vertical servomotor.
        RRE         The Rear Right External servomotor.
        MRH         The Middle Right Horizontal servomotor.
        MRV         The Middle Right Vertical servomotor.
        MRE         The Middle Right External servomotor.
        FRH         The Front Right Horizontal servomotor.
        FRV         The Front Right Vertical servomotor.
        FRE         The Front Right External servomotor.
        RLH         The Rear Left Horizontal servomotor.
        RLV         The Rear Left Vertical servomotor.
        RLE         The Rear Left External servomotor.
        MLH         The Middle Left Horizontal servomotor.
        MLV         The Middle Left Vertical servomotor.
        MLE         The Middle Left External servomotor.
        FLH         The Front Left Horizontal servomotor.
        FLV         The Front Left Vertical servomotor.
        FLE         The Front Left External servomotor.
        all         All the servomotors in the robot.
        H           All Horizontal servomotors in the robot.
        V           All Vertical servomotors in the robot.
        E           All External servomotors in the robot.
        ATripod     The first tripod interface of the robot.
        BTripod     The second tripod interface of the robot.
        ==========  ===========================================================
        """
        self.ssc32 = serial.Serial(serial_path, baud_rate, timeout = timeout)
        self.interval = interval
        self.state = State()
        self.tick = 0

        self.RRH = Rservo(0, init = 1500)
        self.RRV = Rservo(1, center = 1430, init = 1000)
        self.RRE = Rservo(2, init = 1000)

        self.MRH = Rservo(4, init = 1500)
        self.MRV = Rservo(5, center = 1430, init = 1000)
        self.MRE = Rservo(6, init = 1000)

        self.FRH = Rservo(8, init = 1500)
        self.FRV = Rservo(9, init = 1000)
        self.FRE = Rservo(10, init = 1000)

        self.RLH = Lservo(16, init = 1500)
        self.RLV = Lservo(17, center = 1450, init = 2000)
        self.RLE = Lservo(18, init = 2000)

        self.MLH = Lservo(20, init = 1500)
        self.MLV = Lservo(21, center = 1575, init = 2000)
        self.MLE = Lservo(22, center = 1530, init = 2000)

        self.FLH = Lservo(24, init = 1500)
        self.FLV = Lservo(25, center = 1450, init = 2000)
        self.FLE = Lservo(26, init = 2000)

        self.all = [self.RRH, self.RRV, self.RRE,
                    self.MRH, self.MRV, self.MRE,
                    self.FRH, self.FRV, self.FRE,
                    self.RLH, self.RLV, self.RLE,
                    self.MLH, self.MLV, self.MLE,
                    self.FLH, self.FLV, self.FLE]

        self.H = [self.RRH, self.MRH, self.FRH,
                  self.RLH, self.MLH, self.FLH]

        self.V = [self.RRV, self.MRV, self.FRV,
                  self.RLV, self.MLV, self.FLV]

        self.E = [self.RRE, self.MRE, self.FRE,
                  self.RLE, self.MLE, self.FLE]

        self.ATripod = Tripod(self.MRH, self.MRV, self.MRE,
                              self.RLH, self.RLV, self.RLE,
                              self.FLH, self.FLV, self.FLE)

        self.BTripod = Tripod(self.MLH, self.MLV, self.MLE,
                              self.RRH, self.RRV, self.RRE,
                              self.FRH, self.FRV, self.FRE)

        self.controller = KeyboardController()
        #self.controller = CameraController()

        super().__init__()

    down = "#{0}L "
    up = "#{0}H "

    def set_all(self, value):
        for i in self.all:
            i.set_mirror(value)

    def set_allH(self, value):
        for i in self.H:
            i.set_mirror(value)

    def set_allV(self, value):
        for i in self.V:
            i.set_mirror(value)

    def set_allE(self, value):
        for i in self.E:
            i.set_mirror(value)

    def move(self, current = 0, count = 0):
        """
        Update the predicted angles of servomotors in order to move.
        """
        move_left = None
        move_right = None
        a_set_function = None
        b_set_function = None
        if self.state.translation == Translation.forward:
            move_left = Tripod.forward
            move_right = Tripod.forward
            a_set_function = self.ATripod.set_mirror
            b_set_function = self.BTripod.set_mirror
        elif self.state.translation == Translation.backward:
            move_left = Tripod.backward
            move_right = Tripod.backward
            a_set_function = self.ATripod.set_mirror
            b_set_function = self.BTripod.set_mirror
        elif self.state.orientation == Orientation.yaw_right and self.state.translation == Translation.none:
            move_left = Tripod.forward
            move_right = Tripod.forward
            a_set_function = self.ATripod.set_antagonist
            b_set_function = self.BTripod.set_antagonist
        elif self.state.orientation == Orientation.yaw_left and self.state.translation == Translation.none:
            move_left = Tripod.backward
            move_right = Tripod.backward
            a_set_function = self.ATripod.set_antagonist
            b_set_function = self.BTripod.set_antagonist
        else:
            return

        if self.state.orientation == Orientation.yaw_right and self.state.translation != Translation.none:
            move_right = [[(h - 1500) / 2.0 + 1500, v, e] for (h, v, e) in move_right]
        elif self.state.orientation == Orientation.yaw_left and self.state.translation != Translation.none:
            move_left = [[(h - 1500) / 2.0 + 1500, v, e] for (h, v, e) in move_left]

        if count == 0:
            a_set_function(*move_left[self.tick % 4] + move_right[self.tick % 4])
            b_set_function(*move_left[(self.tick + 2) % 4] + move_right[(self.tick + 2) % 4])
        else:
            percent = (current + 1) / count
            a_set_function(*smoothstep(move_left[(self.tick - 1) % 4], move_left[self.tick % 4], percent) + smoothstep(move_right[(self.tick - 1) % 4], move_right[self.tick % 4], percent))
            b_set_function(*smoothstep(move_left[(self.tick + 1) % 4], move_left[(self.tick + 2) % 4], percent) + smoothstep(move_right[(self.tick + 1) % 4], move_right[(self.tick + 2) % 4], percent))

    def send_command(self):
        if self.state.speed < 1.0:
            self.ssc32.write(bytes(''.join([i.get_command() for i in self.all]) + time_string(1, self.interval) + "\r", 'ascii'))
        else:
            self.ssc32.write(bytes(''.join([i.get_command() for i in self.all]) + time_string(self.state.speed, self.interval) + "\r", 'ascii'))

    def pollEvents(self):
        events = []
        nb = self.controller.event_queue.qsize()
        for i in range(nb):
            events.append(self.controller.event_queue.get_nowait())
        return events

    def launch(self):
        cmd = ""
        for i in range(16):
            if i % 4 == 3 or i > 11:
                cmd = cmd + Hexapod.down.format(i)
            else:
                cmd = cmd + Hexapod.up.format(i)
        for i in range(16, 32):
            if i % 4 == 3 or i > 27:
                cmd = cmd + Hexapod.down.format(i)
            else:
                cmd = cmd + Hexapod.up.format(i)
        self.ssc32.write(bytes(cmd + "\r", 'ascii'))
        self.ssc32.write(bytes(''.join([i.get_calibration_command() for i in self.all]) + "\r", 'ascii'))
        self.controller.start()

    def shutdown(self):
        cmd = ""
        for i in range(32):
            cmd = cmd + Hexapod.down.format(i)
        #self.ssc32.write(bytes(cmd + "\r", 'ascii'))

    def run(self):
        """
        This is the main loop of the robot.
        This method should not be called directly, it is called via the start
        method of the Thread class.
        """

        self.launch()
        while (True):
            start = time.perf_counter()
            self.state = State()
            # Scan all pending events
            for event in self.pollEvents():
                self.state.merge(event)
            if self.state.speed < 1.0:
                count = int(1.0 / self.state.speed)
                for i in range(count):
                    # Compute next move to send and update servo angles
                    self.move(i, count)

                    # Wait until current move section finish
                    time_to_wait = max(0, self.interval - (time.perf_counter() - start))
                    time.sleep(time_to_wait)

                    # Send next move to ssc32
                    self.send_command()
                    start = time.perf_counter()
                self.tick = self.tick + 1

            else:
                for i in range(int(self.state.speed)):
                    # Compute next move to send and update servo angles
                    self.move()

                    # Wait until current move section finish
                    time_to_wait = max(0, (self.interval / self.state.speed) - (time.perf_counter() - start))
                    time.sleep(time_to_wait)

                    # Send next move to ssc32
                    self.send_command()
                    self.tick = self.tick + 1
                    start = time.perf_counter()

    def __del__(self):
        """
        Stop the robot, and shutdown energy output to the servomotors.

        .. todo:: Some tests to verify that [ char works.
        """
        self.ssc32.write(bytes("[ \r", 'ascii'))
        self.scc32.write(bytes("STOP \r", 'ascii'))
        self.shutdown()
        self.ssc32.close()
