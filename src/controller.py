#! /usr/bin/env  python3

from queue import Queue
from threading import Thread
from abc import ABCMeta, abstractmethod
from sys import stdin
from src.event import *
import socket
import picamera
import datetime
import time
import io
import colorsys
from PIL import Image
from RPi import GPIO

class Controller(Thread):

    __metaclass__ = ABCMeta
    def __init__(self):
        super().__init__()
        self.event_queue = Queue()
        self.speeds = [ 0.0, 0.10, 0.25, 0.5, 1.0, 2.0, 3.0 ]
        self.speed_index = 4

    @abstractmethod
    def run(self):
        """
        """

class KeyboardController(Controller):
    def run(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(('42.42.42.1', 2048))
        s.listen(1)

        while True:
            conn, addr = s.accept()
            print("Connected by ", addr)

            while True:
                c = conn.recv(1).decode()
                if not c:
                    break

                if c == '8':
                    self.event_queue.put_nowait(State(translation = Translation.forward, speed = self.speeds[self.speed_index]))
                elif c == '2':
                    self.event_queue.put_nowait(State(translation = Translation.backward, speed = self.speeds[self.speed_index]))
                elif c == '4':
                    self.event_queue.put_nowait(State(orientation = Orientation.yaw_left, speed = self.speeds[self.speed_index]))
                elif c == '6':
                    self.event_queue.put_nowait(State(orientation = Orientation.yaw_right, speed = self.speeds[self.speed_index]))
                elif c == '7':
                    self.event_queue.put_nowait(State(translation = Translation.forward, orientation = Orientation.yaw_left, speed = self.speeds[self.speed_index]))
                elif c == '9':
                    self.event_queue.put_nowait(State(translation = Translation.forward, orientation = Orientation.yaw_right, speed = self.speeds[self.speed_index]))
                elif c == '1':
                    self.event_queue.put_nowait(State(translation = Translation.backward, orientation = Orientation.yaw_left, speed = self.speeds[self.speed_index]))
                elif c == '3':
                    self.event_queue.put_nowait(State(translation = Translation.backward, orientation = Orientation.yaw_right, speed = self.speeds[self.speed_index]))

                elif c == '+':
                    if self.speed_index < 6:
                        self.speed_index = self.speed_index + 1
                    self.event_queue.put_nowait(State(speed = self.speeds[self.speed_index]))
                elif c == '-':
                    if self.speed_index > 0:
                        self.speed_index = self.speed_index - 1
                    self.event_queue.put_nowait(State(speed = self.speeds[self.speed_index]))
                elif c == '²':
                    break
            print("Close")
            conn.close()

class CameraController(Controller):
    def __init__(self):
        self.adam7 = []
        for x in range(0, 192, 8):
            for y in range(0, 192, 8):
                self.adam7.append((x, y))
        for x in range(4, 196, 8):
            for y in range(0, 192, 8):
                self.adam7.append((x, y))
        for x in range(0, 192, 8):
            for y in range(4, 196, 8):
                self.adam7.append((x, y))
                self.adam7.append((x + 4, y))
        for x in range(0, 192, 8):
            for y in range(0, 192, 8):
                self.adam7.append((x + 2, y))
                self.adam7.append((x + 6, y))
                self.adam7.append((x + 2, y + 4))
                self.adam7.append((x + 6, y + 4))
        for x in range(0, 192, 8):
            for y in range(0, 192, 8):
                self.adam7.append((x ,y + 2))
                self.adam7.append((x + 2,y + 2))
                self.adam7.append((x + 4,y + 2))
                self.adam7.append((x + 6,y + 2))
                self.adam7.append((x ,y + 6))
                self.adam7.append((x + 2,y + 6))
                self.adam7.append((x + 4,y + 6))
                self.adam7.append((x + 6,y + 6))
        
        GPIO.setmode(GPIO.BOARD)
        super().__init__()

    img_width = 200
    img_height = 200

    def img_handler(self):
        stream = io.BytesIO()
        while True:
            yield stream
            stream.seek(0)
            img = Image.open(stream)
            img = img.convert('HSV')
            px = img.load()
            acc = 0
            nb = 0
            for x in self.adam7:
                h = px[x]
                if h[0] > 226 and h[0] <= 255:#and h[1] > 127:
                    acc = acc + x[0]
                    nb = nb + 1
            if nb > 0:
                self.event_queue.put_nowait(State().create(acc / nb, nb * 4, CameraController.img_width, CameraController.img_height))
                GPIO.setup(16, GPIO.OUT, initial = GPIO.HIGH)
                GPIO.setup(18, GPIO.OUT, initial = GPIO.HIGH)
            else:
                GPIO.setup(16, GPIO.OUT, initial = GPIO.LOW)
                GPIO.setup(18, GPIO.OUT, initial = GPIO.LOW)
            stream.seek(0)
            stream.truncate()

    def run(self):
        with picamera.PiCamera() as camera:
            camera.resolution = (CameraController.img_width, CameraController.img_height)
            camera.framerate = 40
            time.sleep(2)
            camera.capture_sequence(self.img_handler(), 'jpeg', use_video_port=True)
