#!/usr/bin/env python3

import fcntl
import os
import socket
import sys
import termios

HOST = '42.42.42.1'
PORT = 2048

if __name__ == "__main__":
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))
    print("Connected to {} on port {}".format(HOST, PORT))

    fd = sys.stdin.fileno()

    oldterm = termios.tcgetattr(fd)
    newattr = termios.tcgetattr(fd)
    newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
    termios.tcsetattr(fd, termios.TCSANOW, newattr)
    
    oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)
    
    while True:
        data = ''
        try:
            data = sys.stdin.read(1)
        except IOError:
            print("Cannot read stdin")

        if data != '':
            print(data)
            s.send(data.encode())

    print("Close")
    s.close()
    
    termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
    fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)
