.. Momo documentation master file, created by
   sphinx-quickstart on Thu Nov  5 16:13:09 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Momo's documentation!
================================

Contents:

.. toctree::
   :maxdepth: 2

   _rst/modules.rst
   diagrams.rst
   todo.rst
   Wiki <http://momowiki.warlock.ch/>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
